using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Ticket : MonoBehaviour
{
    [SerializeField] private float price;
    [SerializeField] public Text textAmount; 
    //[SerializeField]public Text totalText;
    
    private int amount;
    public float partialPrice;
    public float total;


    #region Methods

    #region Buttons
    public void PlusButton()
    {
        if (amount < 99)
        {
            amount++;
        }

        textAmount.text = amount.ToString();

        SetPartialPrice();
        //SetTotal();
    }

    public void MenusButton()
    {
        if (amount > 0)
        {
            amount--;
        }

        textAmount.text = amount.ToString();

        SetPartialPrice();
        //SetTotal();
    }

    //public void SetTotal()
    //{
    //    total = partialPrice;
    //    //totalText.text = "� " + total;
    //    //print("Total: " + total);
    //}
    #endregion

    public void SetPartialPrice()
    {
        partialPrice = amount * price;
        print("Partial Price: "+partialPrice);
    }

    #endregion
}

