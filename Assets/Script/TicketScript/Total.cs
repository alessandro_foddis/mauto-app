using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Total : MonoBehaviour
{
    public GameObject[] chooseTicketPanels;
    //[SerializeField] private Ticket ticket;
    private float definitiveTotal;
    public Text totalText;
    public GameObject confirmButton;
    public GameObject resetButton;
    public GameObject errorText;
    public GameObject purchasePanel;



    public void SetTotal()
    {

        for (int i = 0; i < chooseTicketPanels.Length; i++)
        {
            definitiveTotal += chooseTicketPanels[i].GetComponent<Ticket>().partialPrice;
        }
        totalText.text = "� " + definitiveTotal.ToString("0.00");

        if (definitiveTotal > 0)
        {
            confirmButton.SetActive(false);
            resetButton.SetActive(true);
        }
        else
        {
            StartCoroutine(ShowErrorButton());
        }

    }

    IEnumerator ShowErrorButton()
    {
        errorText.SetActive(true);
        yield return new WaitForSeconds(2);
        errorText.SetActive(false);
    }

    public void ResetButton()
    {
        SceneManager.LoadScene("TicketScene");
    }

    public void BackToHome()
    {
        SceneManager.LoadScene("UIScene");
    }

    public void PurchaseButton()
    {
        purchasePanel.SetActive(true);
    }

}
