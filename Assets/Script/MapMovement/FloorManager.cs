using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorManager : MonoBehaviour
{
    public GameObject firstFloor;
    public GameObject secondFloor;
    private Vector3 mapPosition = new Vector3(0, 1, 1.3f);
    private Vector3 mapHidden = new Vector3(-15, 1, 1.3f);
    private Vector3 seconMapPosition = new Vector3(0, 1, 0);

    public void FirstFloor()
    {
        secondFloor.transform.position = mapHidden;
        firstFloor.transform.position = mapPosition;
    }
    public void SecondFloor()
    {
        firstFloor.transform.position = mapHidden;
        secondFloor.transform.position = seconMapPosition;
    }
}
