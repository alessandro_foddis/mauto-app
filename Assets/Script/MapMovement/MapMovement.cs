using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMovement : MonoBehaviour
{
    public Camera theCamera;
    protected Plane thePlane;
    public bool rotate;
    private bool isMoving;
    public TestAnimation textAnimation;
    private void Awake()
    {
        if (theCamera == null)
            theCamera = Camera.main;
    }
    private void Update()
    {
        if ((theCamera.transform.localPosition.x < -43 || theCamera.transform.localPosition.x > -37) || (theCamera.transform.localPosition.y < 27.5f || theCamera.transform.localPosition.y > 34) || (theCamera.transform.localPosition.z < -5 || theCamera.transform.localPosition.z > 1.5f))
        {
            isMoving = true;
            theCamera.transform.localPosition = new Vector3(Mathf.Clamp(theCamera.transform.localPosition.x, -43, -37), Mathf.Clamp(theCamera.transform.localPosition.y, 27.5f, 34), Mathf.Clamp(theCamera.transform.localPosition.z, -5, 1.5f));
            Invoke("bIsFalse", 0.3f);
        }
        if (Input.touchCount >= 1)
            thePlane.SetNormalAndPosition(transform.up, transform.position);

        var delta1 = Vector3.zero;
        var delta2 = Vector3.zero;

        if (Input.touchCount >= 1)
        {
            delta1 = PlanePositionDelta(Input.GetTouch(0));
            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                if (isMoving == false)
                {
                    theCamera.transform.Translate((delta1 / 3), Space.World);
                    textAnimation.OffText();
                }
                 
            }
        }

        if (Input.touchCount >= 2)
        {
            var pos1 = PlanePosition(Input.GetTouch(0).position);
            var pos2 = PlanePosition(Input.GetTouch(1).position);
            var pos1b = PlanePosition(Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition);
            var pos2b = PlanePosition(Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition);

            var zoom = Vector3.Distance(pos1, pos2) / (Vector3.Distance(pos1b, pos2b));

            if (zoom == 0 || zoom > 10)
                return;
            
            if (isMoving == false)
            theCamera.transform.position = Vector3.LerpUnclamped(pos1, theCamera.transform.position, (1 / zoom));

            if (rotate && pos2b != pos2)
                theCamera.transform.RotateAround(pos1, thePlane.normal, Vector3.SignedAngle(pos2 - pos1, pos2b - pos1b, thePlane.normal));
        }
    }

    protected Vector3 PlanePositionDelta(Touch theTouch)
    {
        if (theTouch.phase != TouchPhase.Moved)
            return Vector3.zero;

        var rayBefore = theCamera.ScreenPointToRay(theTouch.position - theTouch.deltaPosition);
        var rayNow = theCamera.ScreenPointToRay(theTouch.position);
        if (thePlane.Raycast(rayBefore, out var enterBefore) && thePlane.Raycast(rayNow, out var enterNow))
            return rayBefore.GetPoint(enterBefore) - rayNow.GetPoint(enterNow);

        return Vector3.zero;
    }
    protected Vector3 PlanePosition(Vector2 screenPosition)
    {
        var rayNow = theCamera.ScreenPointToRay(screenPosition);
        if (thePlane.Raycast(rayNow, out var enterNow))
            return rayNow.GetPoint(enterNow);

        return Vector3.zero;
    }
    private void bIsFalse()
    {
        isMoving = false;
    }
}



