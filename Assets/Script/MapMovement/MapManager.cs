using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MapManager : MonoBehaviour
{
    public TestAnimation theAnimation;
    public TextMeshProUGUI text;
    public void MapClick(GameObject objectClicked)
    {
        if(theAnimation.isOpen == true)
        {
            theAnimation.OnText();
        }
        else
        {
            theAnimation.ShowText();
            theAnimation.isOpen = true;
        }
        
        text.text = objectClicked.name;
    }

}
