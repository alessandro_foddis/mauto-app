using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAnimation : MonoBehaviour
{
    public GameObject textSlider;
    public bool isOpen = true;

    public void ShowText()
    {
        if(textSlider != null)
        {
            Animator animator = textSlider.GetComponent<Animator>();
            if(animator != null)
            {
                isOpen = animator.GetBool("show");
                animator.SetBool("show", !isOpen);
            }
        }
    }
    public void OffText()
    {
        if (textSlider != null)
        {
            Animator animator = textSlider.GetComponent<Animator>();
            if (animator != null)
            {
                isOpen = animator.GetBool("show");
                animator.SetBool("show", false);
                isOpen = false;
            }
        }
    }
    public void OnText()
    {
        if (textSlider != null)
        {
            Animator animator = textSlider.GetComponent<Animator>();
            if (animator != null)
            {
                isOpen = animator.GetBool("show");
                animator.SetBool("show", true); ;
                isOpen = true;
            }
        }
    }
}
