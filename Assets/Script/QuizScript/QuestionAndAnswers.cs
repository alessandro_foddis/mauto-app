
[System.Serializable]
public class QuestionAndAnswers
{
    public string question;
    public string[] answers;
    public int correctAnswer;

}
