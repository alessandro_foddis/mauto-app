using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizManager : MonoBehaviour
{
    public List<QuestionAndAnswers> QnA;
    public GameObject[] options;
    public int currentQuestion;
    public GameObject victoryPanel;
    public GameObject quizPanel;
    public Text QuestionText;
    private int index;
    private int totalQuestions;

    private void Start()
    {
        totalQuestions = QnA.Count;
        GenerateQuestion();
        Debug.Log(totalQuestions);

    }

    public void Correct()
    {
        QnA.RemoveAt(currentQuestion);
        GenerateQuestion();
    }

    public void Wrong()
    {
        QnA.RemoveAt(currentQuestion);
        GenerateQuestion();
    }


    void SetAnswers()
    {
        for (int i = 0; i < options.Length; i++)
        {
            options[i].GetComponent<AnswerScript>().isCorrect = false;
            options[i].transform.GetChild(0).GetComponent<Text>().text = QnA[currentQuestion].answers[i];
            options[i].GetComponent<Image>().color = options[i].GetComponent<AnswerScript>().startColor;

            if (QnA[currentQuestion].correctAnswer ==i+1)
            {
                options[i].GetComponent<AnswerScript>().isCorrect = true;
            }
        }
    }

    void GenerateQuestion()
    {

        if(QnA.Count > 0 && index < totalQuestions) //QnA.Count > 0 && index < QnA.Count
        {
            //currentQuestion = 0;

            QuestionText.text = QnA[currentQuestion].question;
            SetAnswers();
            index++;
            print(index);
        }
        else 
        {
            VictoryPanel();
            Debug.Log("Victory");
        }


    }

    void VictoryPanel()
    {
        quizPanel.SetActive(false);
        victoryPanel.SetActive(true);
    }



}
