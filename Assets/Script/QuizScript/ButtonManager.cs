using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    [SerializeField] public GameObject quitPanel; 
    public void QuitButton()
    {
        quitPanel.SetActive(true);
    }

    public void NoButton()
    {
        quitPanel.SetActive(false);
    }

    public void YesButton()
    {
        SceneManager.LoadScene("UIScene");
    }
}
