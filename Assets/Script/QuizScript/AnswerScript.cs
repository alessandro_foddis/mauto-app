using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerScript : MonoBehaviour
{
    public bool isCorrect = false;
    public QuizManager quizManager;
    public Color startColor;

    public void Awake()
    {
        startColor = GetComponent<Image>().color;
    }
    public void Answer()
    {
        if(isCorrect)
        {
            GetComponent<Image>().color = Color.green;
            Debug.Log("Correct Answer");
            StartCoroutine(WaitForNextQuestion());
            //quizManager.Correct();
        }
        else
        {
            GetComponent<Image>().color = Color.red;
            Debug.Log("Worng Answer");
            StartCoroutine(WaitForNextQuestion());
            //quizManager.Wrong();
        }
    }


   IEnumerator WaitForNextQuestion()
    {
        yield return new WaitForSeconds(1.5f);
        quizManager.Correct();
    }

}


