using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderMenu : MonoBehaviour
{
    public GameObject PanelMenu;
    public bool isOpen = true;
    public void ShowHideMenu()
    {
        if (PanelMenu != null)
        {
            Animator animator = PanelMenu.GetComponent<Animator>();

            if (animator != null)
            {
                isOpen = animator.GetBool("Show");
                animator.SetBool("Show", !isOpen);
            }
        }
    }

    public void OnMenu()
    {
        if (PanelMenu != null)
        {
            Animator animator = PanelMenu.GetComponent<Animator>();

            if (animator != null)
            {
                isOpen = animator.GetBool("Show");
                animator.SetBool("Show", true);
                isOpen = true;
            }
        }
    }

    public void OffMenu()
    {
        if (PanelMenu != null)
        {
            Animator animator = PanelMenu.GetComponent<Animator>();

            if (animator != null)
            {
                isOpen = animator.GetBool("Show");
                animator.SetBool("Show", false);
                isOpen = false;
            }
        }
    }


}
