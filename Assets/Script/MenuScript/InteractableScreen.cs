using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InteractableScreen : MonoBehaviour
{
    [SerializeField] private GameObject discoverPanel;
    [SerializeField] private GameObject mapPanel;
    [SerializeField] private GameObject eventsPanel;
    [SerializeField] private GameObject playPanel;
    [SerializeField] private GameObject ticketsPanel;
    [SerializeField] private GameObject interactableScreen;
    [SerializeField] private SliderMenu sliderMenu;
    GameObject DiscoverPanel;
    GameObject MapPanel;
    GameObject EventsPanel;
    GameObject PlayPanel;
    GameObject TicketsPanel;

    public void OpenDiscoverPanel()
    {
        DiscoverPanel = Instantiate(discoverPanel, interactableScreen.transform);
    }
    public void OpenMapPanel()
    {
        SceneManager.LoadScene("InteractibleMap");
    }
    public void OpenEventsPanel()
    {
        SceneManager.LoadScene("EventsScene");
    }
    public void OpenPlayPanel()
    {
        SceneManager.LoadScene("QuizGame");
    }
    public void OpenTicketsPanel()
    {
        SceneManager.LoadScene("TicketScene");
    }
     public void MainMenu()
    {
        SceneManager.LoadScene("UIScene");
    }
    public void OpenHomePanel()
    {
       if (DiscoverPanel != null)
        {
            Destroy(DiscoverPanel);
            sliderMenu.OffMenu();
        }
        if (MapPanel != null)
        {
            Destroy(MapPanel);
            sliderMenu.OffMenu();
        }
       if (EventsPanel != null)
        {
            Destroy(EventsPanel);
            sliderMenu.OffMenu();
        }
       if (PlayPanel != null)
        {
            Destroy(PlayPanel);
            sliderMenu.OffMenu();
        }
       if (TicketsPanel != null)
        {
            Destroy(TicketsPanel);
            sliderMenu.OffMenu();
        }
        if (DiscoverPanel == null && MapPanel == null && EventsPanel == null && PlayPanel == null && TicketsPanel == null)
        {
            sliderMenu.OffMenu();
        }
    }
    public void Contatti()
    {
        Application.OpenURL("https://www.museoauto.com/contatti/");
    }
    public void HamburgherDicover()
    {
        if (DiscoverPanel == null)
        {
            DiscoverPanel = Instantiate(discoverPanel, interactableScreen.transform);
        }
        sliderMenu.ShowHideMenu();
    }
    public void HamburgherMap()
    {
        if (MapPanel == null)
        {
            SceneManager.LoadScene("InteractibleMap");
        }
        sliderMenu.ShowHideMenu();
    }
    public void HamburgherEvents()
    {
        if (EventsPanel == null)
        {
            SceneManager.LoadScene("EventsScene");
        }
        sliderMenu.ShowHideMenu();
    }
    public void HamburgherPlay()
    {
        if (PlayPanel == null)
        {
            SceneManager.LoadScene("QuizGame");
        }
        sliderMenu.ShowHideMenu();
    }
    public void HamburgherTickets()
    {
        if (TicketsPanel == null)
        {
            SceneManager.LoadScene("TicketScene");
        }        sliderMenu.ShowHideMenu();    }

}
